#!/usr/bin/python3
# -*- coding: utf-8 -*-
import utils.GlobalVariables as globalVars
import dao.mongodb_connector as mongodbconnector
import utils.SystemProperties as systemproperties


mongodbShodanSharedProperties  = None


def getShodanSharedProperties():
    global mongodbShodanSharedProperties
    if mongodbShodanSharedProperties is None:
        hostmongo  = systemproperties.get_hostmongo()
        portmongo  = int(systemproperties.get_portmongo())
        dbShodan   = systemproperties.get_dbShodan()
        colShodan  = systemproperties.get_colShodan()
        mongodbShodanSharedProperties = mongodbconnector.mongoDBConnection(hostmongo, portmongo, dbShodan, colShodan)
    return mongodbShodanSharedProperties


def getValue(key):
    value = ""
    try:
        mongodb = getShodanSharedProperties()
        value = mongodb.find_one({globalVars.key: key})[globalVars.value]
    except:
        print("Value of the property " + key + " not exists.") 
    return value


def setKeyValue(key, value):
    mongodb = getShodanSharedProperties()
    if (mongodb.find_one({globalVars.key: key})):
        mongodb.update_one({globalVars.key: key}, {"$set": {globalVars.value: value}}, upsert=False)
    else:
        data = {globalVars.key: key, globalVars.value: value}
        mongodb.insert(data)