import os
projectPath  = os.path.join(os.path.dirname(__file__))
import logging
logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s')
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from gui.alertsystemgui import Ui_MainWindow
import controller.ShodanProcess as shodanProcess
import controller.KibanaFunctions as kibanafunctions
import alerts.MailAlert as alertmail
import alerts.TelegramAlert as alerttelegram
import utils.GlobalVariables as globalVars
import correlate.dataset as dataset 
import sys
import time


company_name = None


class ProgressbarThread(QtCore.QThread):
    progressSignal = QtCore.pyqtSignal(int)
    completeSignal = QtCore.pyqtSignal(str)
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent=parent)
        self.maxRange           = 100
        self.completionMessage  = "done."
    def run(self):
        for i in range(self.maxRange):
            time.sleep(5)
            self.progressSignal.emit(i)
        self.completeSignal.emit(self.completionMessage)

class StartServicesThread(QtCore.QThread):
    completeSignal = QtCore.pyqtSignal(str)
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent=parent)
        self.completionMessage  = "Services Started"
    def run(self):
        os.system(globalVars.scriptServicesStart)
        self.completeSignal.emit(self.completionMessage)

class SearchCompanyThread(QtCore.QThread):
    completeSignal = QtCore.pyqtSignal(str)
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent=parent)
        self.completionMessage  = "Search Started"
    def run(self):
        shodanProcess.searchAndSave2Elastic(company_name)
        self.completeSignal.emit(self.completionMessage)

class CorrelateDataThread(QtCore.QThread):
    completeSignal = QtCore.pyqtSignal(str)
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent=parent)
        self.completionMessage  = "Correlation Started"
    def run(self):
        dataset.correlation()
        self.completeSignal.emit(self.completionMessage)
        

class SystemAlertApp(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self)
        
        self.disableButton()
        self.threadStartservices = StartServicesThread()
        self.threadStartservices.completeSignal.connect(self.handle_thread_refresh)
        self.threadStartservices.start()
        
        self.threadProgressbar   = ProgressbarThread()
        self.threadProgressbar.completionMessage = "Completed Process"
        self.threadProgressbar.completeSignal.connect(self.handle_thread_completion)
        self.threadProgressbar.progressSignal.connect(self.handle_progress_bar)
        
        self.threadSearchCompany = SearchCompanyThread()
        self.threadSearchCompany.completionMessage = "Completed Process"
        self.threadSearchCompany.completeSignal.connect(self.handle_alerts)

        self.threadCorrelate = CorrelateDataThread()
        self.threadCorrelate.completionMessage = "Completed Process"
        self.threadCorrelate.completeSignal.connect(self.handle_alerts)
        
        self.start_button.clicked.connect(self.start_threads)
        self.cor_button.clicked.connect(self.start_correlation)


    def enableButtonAndTabs(self):
        self.start_button.setEnabled(True)
        self.start_button.setStyleSheet("background-color: green")        
        self.start_button.setText("Search")
        self.refreshTabsView()
    def disableButton(self):
        self.start_button.setEnabled(False)
        self.start_button.setStyleSheet("background-color: red")
        self.start_button.setText("Running...")
    def refreshTabsView(self):
        #self.webEngineViewShodan.setUrl(QtCore.QUrl(globalVars.fileURL + projectPath + globalVars.HTMLShodanControl))
        #self.webEngineViewCVEs.setUrl(QtCore.QUrl(globalVars.fileURL + projectPath + globalVars.HTMLShodanCVEs))
        self.webEngineViewShodan.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/ShodanControl.html'))
        self.webEngineViewCVEs.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/ShodanCVEs.html'))
        self.webEngineViewSuricata.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/suricata.html'))
        self.webEngineViewSnort.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/snort.html'))
        self.webEngineViewOssec.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/ossec.html'))
        self.webEngineViewWazuh.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/wazuh.html'))
        self.webEngineViewCorrelation.setUrl(QtCore.QUrl('file:///home/U406577/git/alertatemprana/2.AlertaTemprana/html/correlation.html'))
    def checkAlerts(self):
        if self.mail_check.isChecked() == True:
            alertmail.mailSend()
        if self.telegram_check.isChecked() == True:
            alerttelegram.send_alert()

    def start_correlation(self):
        self.threadCorrelate.start()
    
    def start_threads(self):
        global company_name
        self.results_window.append("Searching on Shodan...")
        company_name = self.company_name.toPlainText().lower()
        self.disableButton()
        self.threadProgressbar.start()
        self.threadSearchCompany.start()

    @QtCore.pyqtSlot(int)
    def handle_progress_bar(self, e):
        self.progressBar.setValue(e)

    @QtCore.pyqtSlot(str)
    def handle_thread_completion(self, e):
        pass

    @QtCore.pyqtSlot(str)
    def handle_thread_refresh(self, e):
        self.enableButtonAndTabs()
    
    @QtCore.pyqtSlot(str)
    def handle_alerts(self, e):
        kibanafunctions.loadKibana()
        self.checkAlerts()
        self.enableButtonAndTabs()
        self.progressBar.setValue(0)
        QtWidgets.QMessageBox.information(self, "Done", e)
    
    def closeEvent(self, event):
        reply = QtWidgets.QMessageBox.question(self, 'Message', 
            "Are you sure to quit?", QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
            os.system(globalVars.scriptServicesStop)
        else:
            event.ignore()
            

if __name__ == '__main__':
    
    """ If receive params start from console else run GUI """  
    if (len(sys.argv) > 1):
        companyName = sys.argv[1]
        shodanProcess.searchAndSave2Elastic(companyName)
        kibanafunctions.loadKibana()
    
    else:
        app = QtWidgets.QApplication(sys.argv)
        window = SystemAlertApp()
        window.show()
        sys.exit(app.exec_())
