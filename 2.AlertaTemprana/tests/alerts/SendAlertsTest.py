'''
Created on Apr 10, 2018

@author: Monospow
'''
import alerts.TelegramAlert as telegramalert
import utils.GlobalVariables as globalVars
import alerts.MailAlert as mailalert
import utils.SystemProperties as systemproperties
import time
import os
import unittest


#Global tests variables
impact = "partial"
alert  = "0"


class TestAlertTelegramMethods(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        os.system(globalVars.scriptServicesStart)
        time.sleep(10)
        
    def testSendMailAlert(self):
        if systemproperties.get_mailEnabled() == "true":
            mailalert.mailSend()
        self.assertTrue(True)

    def testSendTelegramAlert(self):    
        if systemproperties.get_telegramEnabled() == "true":
            telegramalert.send_message_cid()
        self.assertTrue(True)
    
    @classmethod
    def tearDownClass(cls):
        os.system(globalVars.scriptServicesStop)
        

if __name__ == '__main__':
    unittest.main()