'''
Created on Apr 10, 2018

@author: Monospow
'''
import alerts.TelegramAlert as telegramalert
import utils.GlobalVariables as globalVars
import dao.mongodb_manager as mongodb_manager
import time
import os
import unittest


#Global tests variables
impact = "partial"
alert  = "0"


class TestAlertTelegramMethods(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        os.system(globalVars.scriptServicesStart)
        time.sleep(10)
        
        
    def testSetKeyValue(self):
        global impact
        global alert
        mongodb_manager.setKeyValue(globalVars.impact, impact)
        mongodb_manager.setKeyValue(globalVars.alert,  alert)
        self.assertTrue(True)
    
    def testGetValue(self):
        global impact
        global alert
        dbimpact = mongodb_manager.getValue(globalVars.impact)
        dbalert  = mongodb_manager.getValue(globalVars.alert)
        self.assertNotEqual(dbimpact, None, "DB is empty")
        self.assertNotEqual(dbalert,  None, "DB is empty")
    
    def testAlertSystem(self):
        telegramalert.send_alert()
        self.assertTrue(True)
    
    @classmethod
    def tearDownClass(cls):
        os.system(globalVars.scriptServicesStop)
        

if __name__ == '__main__':
    unittest.main()