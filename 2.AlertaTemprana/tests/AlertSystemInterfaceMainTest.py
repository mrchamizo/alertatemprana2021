import os
default_path = os.path.join(os.path.dirname(__file__), '..')
os.chdir(default_path)
import AlertSystemMain as alertsystemmain
import utils.GlobalVariables as globalVars
from PyQt5 import QtWidgets
import sys
import time
import unittest


class TestAlertSystemMethods(unittest.TestCase):
    
    window = None
    
    @classmethod
    def setUpClass(cls):
        os.system(globalVars.scriptServicesStart)
        time.sleep(10)
    
    def testInitMainWindows(self):
        os.system(globalVars.mongodbprocess)
        app = QtWidgets.QApplication(sys.argv)
        window = alertsystemmain.SystemAlertApp()
        window.show()
        sys.exit(app.exec_())

    def testSetValuesMainWindows(self):
        global window
        company_name = "Bankia"
        window.company_name.append(company_name)
        window.SearchCompany
        self.assertTrue(True)
    
    @classmethod
    def tearDownClass(cls):
        os.system(globalVars.scriptServicesStop)