import os
default_path = os.path.join(os.path.dirname(__file__), '..')
os.chdir(default_path)
import controller.ShodanProcess as shodanProcess
import controller.KibanaFunctions as kibanafunctions
import tests.utils.SystemGeneralFunctionsTests as systemgeneralfunctions
import utils.GlobalVariables as globalVars
import json
import time
import unittest



class Json2KiabanaTest(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        os.system(globalVars.scriptServicesStart)
        time.sleep(10)


    def testJson2Kibana(self):
        json_file  = default_path + "/resources/iberdrola.json"
        
        shodan_data = []
        for line in open(json_file):
            shodan_data.append(json.loads(line))
        
        #init CVEs and Elastic Mappings
        shodanProcess.initCVEandMappings()
        
        #Search and Save to Elastic and view in Kibana
        shodanProcess.loadPage2Elastic(shodan_data)
        kibanafunctions.loadKibana()
        systemgeneralfunctions.launch_browsers()
        time.sleep(40)
        

    @classmethod
    def tearDownClass(cls):
        os.system(globalVars.scriptServicesStop)