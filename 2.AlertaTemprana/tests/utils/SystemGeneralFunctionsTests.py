import os
projectPath  = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
import time
import webbrowser
import utils.GlobalVariables as globalVars


def launch_browsers():
    webbrowser.open_new_tab(projectPath + globalVars.HTMLShodanControl)
    time.sleep(5)
    webbrowser.open_new_tab(projectPath + globalVars.HTMLShodanCVEs)