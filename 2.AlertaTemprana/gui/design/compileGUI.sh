#!/bin/bash

cd /home/auditor/git/alertatemprana/2.AlertaTemprana/gui/design
pyrcc5 alertsystemimages.qrc -o ../alertsystemimages_rc.py
pyuic5 -x alertsystemgui.ui -o ../alertsystemgui.py

#Replace alertsystemimages_rc by the package gui.alertsystemimages_rc
sed -i '/import alertsystemimages_rc/c\import gui.alertsystemimages_rc' /home/auditor/git/alertatemprana/2.AlertaTemprana/gui/alertsystemgui.py