# Processes Shodan to Elastic
'''
Created on Mar 10, 2018

@author: Monospow
'''
import utils.SystemFunctions as systemfunctions
import sys


def mappingsParsingCorrect(service_line):
    """Correct mapping JSON to load on Elastic Stack"""
    
    keyType = "shodan"
    try:
        #Change "ip" to "ip_int" and "ip_str" to "ip"
        ip_int = service_line['ip']
        del service_line['ip']
        service_line['ip'] = service_line['ip_str']
        del service_line['ip_str']
        service_line['ip_int'] = ip_int
        
    except KeyError:
        try:
            service_line['ip'] = service_line['ip_str']
            del service_line['ip_str']
        except KeyError:
            print(service_line)
            print('Missing required \'ip\' field in the element above. Exiting now...')
            sys.exit(1)

    #Convert ssl.cert.serial to string if exist
    try:
        service_line['ssl']['cert']['serial'] = str(service_line['ssl']['cert']['serial'])
    except KeyError:
        pass
    
    #Convert ssl.dhparams.generator to string if exist
    try:
        service_line['ssl']['dhparams']['generator'] = str(service_line['ssl']['dhparams']['generator'])
    except (KeyError, TypeError):
        pass

    try:
        #Rename shodan.modules to protocols
        service_line['protocols'] = service_line['_shodan']['module']
        service_line['id'] = abs(service_line['hash'])
        #Delete the rest.   
        del service_line['_shodan']
    except KeyError:
        pass
   
    #Convert "asn" to integer
    try:
        service_line['asn'] = int((service_line['asn'])[2:])
        #Rename "location.country_name" to "location.country"
        service_line['location']['country'] = service_line['location']['country_name']
        del service_line['location']['country_name']
        #Rename latitude and longitude for geoip (work in ElasticSearch)
        service_line['location']['geo'] = {}
        service_line['location']['geo']['lat'] = service_line['location']['latitude']
        service_line['location']['geo']['lon'] = service_line['location']['longitude']
        service_line['location']['geohash'] = systemfunctions.convert2geohash(service_line['location']['geo']['lat'],service_line['location']['geo']['lon'])
        del service_line['location']['latitude']
        del service_line['location']['longitude']
    except KeyError:
        pass

    #Limit the number of fields
    service_line = nestElements(service_line)

    #Put prefix non-nested fields with search motor ('shodan')
    service_line = systemfunctions.add_prefix(service_line, keyType)
    return service_line



def nestElements(service_line):
    """Nested elements that JSON elements contained to one element"""

    try:
        service_line['http']['components'] = str(
            service_line['http']['components'])
    except KeyError:
        pass
    try:
        service_line['elastic'] = str(
            service_line['elastic'])
    except KeyError:
        pass
    return service_line