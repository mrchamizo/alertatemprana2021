#URLs
URLElastic                  = "http://localhost:9200"
URLKibana                   = "http://localhost:5601"
#URLcvesearch                = "https://cve.circl.lu"
URLcvesearch                = "http://127.0.0.1:5000/"
URLTelegramBot              = "https://api.telegram.org/bot"
SMTP_GMAIL                  = "smtp.gmail.com: 587"
URLElasticMappingAlert      = URLElastic + "/_template/mapping_alerta"
URLKibanaDefaultIndex       = URLKibana + "/api/kibana/settings/defaultIndex"
URLKibanaIndexPattern       = URLKibana + "/api/saved_objects/index-pattern"
fileURL                     = "file://"

#Elastic
elasticIndexName            = "shodan"


#Shodan
prefixSearchByCompany       = "org:"
prefixDocType               = "doc"


#MESSAGES
NO_INFO                     = "NO_INFO"
CETA_MESSAGE                = "CETA Alert System"
SUCESS_EMAIL                = "successfully sent email to %s:"


#PATHS
cvefinder                   = "/cve-search/bin/search.py"
HTMLShodanControl           = "/html/ShodanControl.html"
HTMLShodanCVEs              = "/html/ShodanCVEs.html"
pathPropertiesKibanaPanel   = "/resources/kibanaMainPanel.json"
pathShodanIndexPattern      = "/resources/shodanIndexPattern.json"
pathShodanProperties        = "/properties/shodan.properties"
pathAlertsProperties        = "/properties/alerts.properties"
pathMongoProperties         = "/properties/mongodb.properties"


#COMMANDS
mongodbprocess              = "mate-terminal --title=MongoDB --geometry=80x4+1200+0 -x sh -c '/home/auditor/Desktop/mongodb/bin/mongod --dbpath /home/auditor/Desktop/mongodb/data/' &"
elasticprocess              = "mate-terminal --title=Elastic --geometry=80x4+1200+160 -x sh -c '/home/auditor/Desktop/elasticsearch/bin/elasticsearch' &"
telegrambotprocess          = "mate-terminal --title=TelegramBot --geometry=80x4+1200+480 -x sh -c 'python3 /home/auditor/git/alertatemprana/3.AlertaTempranaBot/TelegramBotMain.py' &"
scriptServicesStart         = "sh /home/U406577/git/alertatemprana/1.Scripts/1.startServices.sh"
scriptServicesStop          = "sh /home/U406577/git/alertatemprana/1.Scripts/2.stopServices.sh"


#CONSTANTS
shodanKey                   = "API_KEY"
key                         = "KEY"
value                       = "VALUE"
shodanResultsPerPage        = 100

impact                      = "impact"
alert                       = "alert"
alarm                       = "alarm"
cid                         = "cid"
count                       = "count"
sendMail                    = "sendMail"
sendTele                    = "sendTele"
typeSearch                  = "typeSearch"
companyName                 = "companyName"

mail_from                   = "mail_from"
mail_to                     = "mail_to"
mail                        = "MAIL"
pwd                         = "PWD"
