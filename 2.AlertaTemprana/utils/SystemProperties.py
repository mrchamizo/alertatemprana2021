# General properties from the system
import os
projectPath  = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
import utils.GlobalVariables as globalVars
import configparser
import shodan



#Read Properties
configAlerts = configparser.ConfigParser()
configAlerts.read(projectPath + globalVars.pathAlertsProperties)
configMongoDB = configparser.ConfigParser()
configMongoDB.read(projectPath + globalVars.pathMongoProperties)
configShodan = configparser.ConfigParser()
configShodan.read(projectPath + globalVars.pathShodanProperties)

#Read Alerts Properties
token           = configAlerts.get('telegram', 'TOKEN')
mailfrom        = configAlerts.get(globalVars.mail_from, globalVars.mail)
pwdfrom         = configAlerts.get(globalVars.mail_from, globalVars.pwd)
mailto          = configAlerts.get(globalVars.mail_to,   globalVars.mail)
mailEnabled     = configAlerts.get(globalVars.mail_to,   "ENABLED")
telegramEnabled = configAlerts.get("telegram",   "ENABLED")

#Read MongoDB Properties
hostmongo       = configMongoDB.get('mongoDB', 'hostmongo')
portmongo       = int(configMongoDB.get('mongoDB', 'portmongo'))
dbShodan        = configMongoDB.get('mongoDB', 'dbShodan')
colShodan       = configMongoDB.get('mongoDB', 'colShodan')

#Read shodan Properties
shodanApikey    = shodan.Shodan(configShodan.get('shodan', 'API_KEY'))

#Generals
index_name      = ""
doc_type        = ""
company_name    = ""



def get_token():
    global token
    return token

def get_mailfrom():
    global mailfrom
    return mailfrom

def get_pwdfrom():
    global pwdfrom
    return pwdfrom

def get_mailto():
    global mailto
    return mailto

def get_mailEnabled():
    global mailEnabled
    return mailEnabled

def get_telegramEnabled():
    global telegramEnabled
    return telegramEnabled

def get_hostmongo():
    global hostmongo
    return hostmongo

def get_portmongo():
    global portmongo
    return portmongo

def get_dbShodan():
    global dbShodan
    return dbShodan

def get_colShodan():
    global colShodan
    return colShodan

def get_shodanApikey():
    global shodanApikey
    return shodanApikey

def set_indexname(index):
    global index_name
    index_name = index

def get_indexname():
    return index_name

def set_doc_type(doctype):
    global doc_type
    doc_type = doctype

def get_doc_type():
    return doc_type

def set_company_name(company):
    global company_name
    company_name = company

def get_company_name():
    return company_name