'''
Created on Mar 27, 2018

@author: Monospow

Adapted from: CVE-Search 
'''
import requests
from urllib.parse import urljoin
import utils.GlobalVariables as globalVars


class CVESearch(object):

    def __init__(self, base_url=globalVars.URLcvesearch, proxies=None):
        self.base_url = base_url
        self.session = requests.Session()
        self.session.proxies = proxies
        self.session.headers.update({
            'content-type': 'application/json',
            'User-Agent': 'CETA_CVE - python wrapper'})
    
    
    def _http_get(self, api_call, query=None):
        if query is None:
            response = self.session.get(urljoin(self.base_url, 'api/{}'.format(api_call)))
        else:
            response = self.session.get(urljoin(self.base_url, 'api/{}/{}'.format(api_call, query)))
        return response

    def _http_get1(self, api_call, query=None, query1=None):
        if query is None:
            response = self.session.get(urljoin(self.base_url, 'api/{}'.format(api_call)))
        else:
            response = self.session.get(urljoin(self.base_url, 'api/{}/{}/{}'.format(api_call, query, query1)))
        return response
  
  
  
    def search(self, param1, param2):
        """Return a dict containing all the vulnerabilities per vendor and a specific product"""
        data = self._http_get1('search', query=param1, query1=param2)
        return data.json()
 
 
 
    def cvefor(self, param):
        """Returns a dict containing the CVE's for a given CPE ID"""
        data = self._http_get('cvefor', query=param)
        return data.json()
