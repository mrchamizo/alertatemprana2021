# General functions of the system

#Alphabet described in IETF's RFC 4648 (http://tools.ietf.org/html/rfc4648)
#Alphabet in geohash is different from common base32 alphabet
__base32    = '0123456789bcdefghjkmnpqrstuvwxyz'
__decodemap = { }

for i in range(len(__base32)):
    __decodemap[__base32[i]] = i
del i


def add_prefix(service_line, keyType):
    """The fields are grouped in the nested by service and protocol, nested by service and not nested"""
   
    keys_not_search_source = ['asn', 'ip', 'ip_int', 'geoip']
    
    for key in list(service_line):
        # prefix all non-nested elements except ip and ip_int
        if '.' not in key and key not in keys_not_search_source:
            new_key = key.replace(key, (keyType + "." + key))
            if new_key != key:
                service_line[new_key] = service_line[key]
                del service_line[key]
    return service_line



def emptyClean(service_line):
    """Removing the empty elements from service_dict (element)"""
        
    if isinstance(service_line, list):
        return [v for v in (emptyClean(v) for v in service_line) if v]
    if not isinstance(service_line, (dict, list)):
        return service_line
    return {k: v for k, v in ((k, emptyClean(v)) for k, v in service_line.items()) if v or v == 0}



def convert2geohash (latitude, longitude, precision=12):
    """    
    Encode a position given in float arguments latitude, longitude to
    a geohash which will have the character count precision.
    """
    
    lat_interval, lon_interval = (-90.0, 90.0), (-180.0, 180.0)
    geohash = []
    bits = [ 16, 8, 4, 2, 1 ]
    bit = 0
    ch = 0
    even = True
    while len(geohash) < precision:
        if even:
            mid = (lon_interval[0] + lon_interval[1]) / 2
            if longitude > mid:
                ch |= bits[bit]
                lon_interval = (mid, lon_interval[1])
            else:
                lon_interval = (lon_interval[0], mid)
        else:
            mid = (lat_interval[0] + lat_interval[1]) / 2
            if latitude > mid:
                ch |= bits[bit]
                lat_interval = (mid, lat_interval[1])
            else:
                lat_interval = (lat_interval[0], mid)
        even = not even
        if bit < 4:
            bit += 1
        else:
            geohash += __base32[ch]
            bit = 0
            ch = 0
    return ''.join(geohash)