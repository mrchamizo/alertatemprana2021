'''
Created on Apr 4, 2018

@author: Monospow
'''
import controller.ElasticFunctions as elasticfunctions
import controller.CVEFunctions as cvefunctions
import utils.GlobalVariables as globalVars
import utils.SystemProperties as systemproperties
import dao.mongodb_manager as mongodb_manager
import requests


token       = systemproperties.get_token()
telegramURL = globalVars.URLTelegramBot + token + '/sendMessage'

def send_alert():
    alert = mongodb_manager.getValue(globalVars.alert)
    if alert == "1":
        send_message_cid()


def send_message_cid():
    global telegramURL
    mongodb_manager.setKeyValue(globalVars.impact, cvefunctions.get_impact())
    res   = elasticfunctions.getElasticIndex(globalVars.elasticIndexName)
    count = mongodb_manager.getValue(globalVars.count)
    cid   = mongodb_manager.getValue(globalVars.cid)
    texto = 'CETA scan has been executed, we found     ' + str(count) + ' vulnerabilities and processed ' + str(res['hits']['total'])
    data  = [
        ('chat_id', cid),
        ('text',    texto),
    ]
    response = requests.post(telegramURL, data=data)
    print (response)

"""
def send_message_canal():
    global telegramURL
    mongodb_manager.setKeyValue(globalVars.count, str(elasticfunctions.getElasticCount()))
    data = [
        ('chat_id', '@NOMBRECANAL'),
        ('text', 'There is a new vulnerability scan')
    ]
    response = requests.post(telegramURL, data=data)
    print (response)
"""