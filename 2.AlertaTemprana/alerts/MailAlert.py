'''
Created on Apr 2, 2018

@author: Monospow
'''
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import controller.CVEFunctions as cvefunctions
import utils.SystemProperties as systemproperties
import utils.GlobalVariables as globalVars



def mailSend(): 
    """Send mail to the admin when the job is finished"""
    
    #Create message object instance
    msg = MIMEMultipart()

    message = cvefunctions.get_impact()
 
    #Setup the parameters of the message
    password       = systemproperties.get_pwdfrom() 
    msg['From']    = systemproperties.get_mailfrom()
    msg['To']      = systemproperties.get_mailto()
    msg['Subject'] = globalVars.CETA_MESSAGE

    #Add in the message body
    msg.attach(MIMEText(message, 'plain'))

    #Create server
    server = smtplib.SMTP(globalVars.SMTP_GMAIL)
    server.starttls()
    server.login(msg['From'], password)
    server.sendmail(msg['From'], msg['To'], msg.as_string())
    server.quit()
     
    print (globalVars.SUCESS_EMAIL % (msg['To']))