#Kibana functions
'''
Created on Mar 16, 2018

@author: Monospow
'''
import os
projectPath  = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
import controller.ElasticFunctions as elasticFunctions
import utils.GlobalVariables as globalVars
import requests
import json
import asyncio
import sys
import functools



def changeIndex(data, idM, typ):
    if typ == "dashboard":
        count =0 
        dato = json.loads(data['attributes']['panelsJSON'])
        longitud = dato.__len__()
        while (count < longitud):
            dato_id =dato[count]['id']
            dato[count]['id']=dato_id[0:14]+idM[14:36]
            count = count +1 
        data['attributes']['panelsJSON']= json.dumps(dato)
    else:
        if data['attributes']['title'] == "Control_select":
            count =0 
            dato = json.loads(data['attributes']['visState'])
            date = dato['params']['controls']
            longitud = date.__len__()
            while (count < longitud):
                date[count]['indexPattern']=idM
                count = count +1 
            dato['params']['controls']=date
            data['attributes']['visState']= json.dumps(dato)
        else:    
            dato = json.loads(data['attributes']['kibanaSavedObjectMeta']['searchSourceJSON'])
            dato['index']=idM 
            data['attributes']['kibanaSavedObjectMeta']['searchSourceJSON']= json.dumps(dato)
    return data
    
    
    
async def upload_kibana_saved_json(kibana_base_url, jsonArray, idM):
    """Load Visualizations, Dashboards and searchs from KibanaMainPanel.json"""
    
    eventloop = asyncio.get_event_loop()
    
    s = requests.session() # HTTP session for keep-alive
    futures = []
    for obj in jsonArray:
        typ = obj['_type']
        if typ not in ['dashboard', 'search', 'visualization']:
            print('Ignoring unknown Kibana export type: {}'.format(typ), file=sys.stderr)
            continue
        else:
            # All `typ` cases we handle above happen to have _id and _source fields.
            idi = obj['_id'][0:14]+idM[14:36]
            #idi = obj['_id'][0:14]+idM
            print(idi)
            print('Processing {}: {}'.format(typ, idi))

            data = {'attributes': obj['_source']}
            headers = {'kbn-xsrf': 'anything'}
            data = changeIndex(data, idM, typ)
           
            
            post_fun = functools.partial(
                s.post,
                kibana_base_url + '/api/saved_objects/{}/{}?overwrite=true'.format(typ, idi),
                json=data, headers=headers, stream=False)

            future = eventloop.run_in_executor(None, post_fun)
            futures.append(future)

    for response in await asyncio.gather(*futures):
        response.raise_for_status()



def upload_kibana_index_pattern(kibana_base_url):
    """Upload index pattern to Kibana"""
    
    s = requests.Session()
    headers = {
        'Content-Type': 'application/json',
        'kbn-xsrf': 'true',
    }
    data = open(projectPath + globalVars.pathShodanIndexPattern)
    response = s.post(kibana_base_url + '/api/saved_objects/index-pattern/' + globalVars.elasticIndexName, headers=headers, data=data)
    response.connection.close()
    print(response)



def setDefaultIndexPattern():
    """Set default index pattern"""
    
    s = requests.Session()
    headers = {
        'Content-Type': 'application/json',
        'kbn-xsrf': 'anything',
    }
    data = '{"value":"shodan"}'
    response = s.post(globalVars.URLKibanaDefaultIndex, headers=headers, data=data)
    print(response)
    response.connection.close()
    



def loadKibana():
    """Load Kibana files and configuration"""
    
    jsonfile = projectPath + globalVars.pathPropertiesKibanaPanel
    
    with open(jsonfile) as json_data:
        jsonArray = json.load(json_data)

    upload_kibana_index_pattern(globalVars.URLKibana)
    setDefaultIndexPattern()
    index_patt = requests.get(globalVars.URLKibanaIndexPattern + '/' + globalVars.elasticIndexName)
    print(json.loads(index_patt.text))
    idM = ((json.loads(index_patt.text)))['id']
    print(idM)
    #index_patt = requests.get(globalVars.URLKibanaIndexPattern)
    #print(jsonArray)
    #idM = ((json.loads(index_patt.text))['saved_objects'])[0]['id']
    
    asyncio.get_event_loop().run_until_complete(upload_kibana_saved_json(globalVars.URLKibana, jsonArray, idM))
    elasticFunctions.setElasticMappings()
