'''
Created on Mar 27, 2018

@author: Monospow
'''
import utils.CVECore as cveCore
import utils.GlobalVariables as globalVars
import os


aval1 = 0
aval2 = 0
aval3 = 0
aval4 = 0
conf1 = 0
conf2 = 0
conf3 = 0
conf4 = 0
inte1 = 0
inte2 = 0
inte3 = 0
inte4 = 0

cve   = None


def getCVE():
    global cve
    if not cve:
        cve = cveCore.CVESearch()


def cveSearch(service):
    #print(service)
    try:
        os = service["os"]
    except Exception:
        os = ""
    try:
        product =service["product"]
    except Exception:
        product = ""
    try:
        cpe = service['cpe'][0]
    except Exception:
        cpe = ""
    try:   
        server = service['http']['server']
        if server is None:
            server = ""
    except Exception:
        server = ""
    
    resultado  = {}
    resultado2 = {}
    findby     = "no_found"
    
    if (cpe != ""):
        resultado = cve.cvefor(cpe)
        findby = "cpe"
        if product != "":
           
            product    = product.replace("/",":")
            vendor     = cpe.split(":")[2]
            product    = cpe.split(":")[3]
        
            resultado2 = cve.search(vendor,product)
            
            findby     = findby + "product"
            #mappResult(resultado, resultado2)
 
    if resultado:
        service['cve']= resultado
    return service



def mappResult(result1, result2):
    pos = 0
    for cve in result1:
        pos2 = 0
        #result1[pos]['access']={}
        #result1[pos]['impact']={}
        #for cve_pos in result2['results']:
        #    if cve['id'] == cve_pos['id']:
        #        try:
        #            result1[pos]['access'] = result2['results'][pos2]['access']
        #        except Exception:
        #            result1[pos]['access']['authentication'] = globalVars.NO_INFO
        #            result1[pos]['access']['complexity']     = globalVars.NO_INFO
        #            result1[pos]['access']['vector']         = globalVars.NO_INFO
        #        try:
        #            result1[pos]['impact'] = result2['results'][pos2]['impact']
        #        except Exception:
        #            result1[pos]['impact']['availability']   = globalVars.NO_INFO
        #            result1[pos]['impact']['confidentiality']= globalVars.NO_INFO
        #            result1[pos]['impact']['integrity']      = globalVars.NO_INFO
        #        break
        #    pos2 = pos2 + 1

        #try:
        #    result1[pos]['cvss']    = str(result1[pos]['cvss'])
        #except  Exception:
        #    result1[pos]['cvss']    = globalVars.NO_INFO
        #try:
        #    result1[pos]['cwe']     = str(result1[pos]['cwe'])
        #except  Exception:
        #    result1[pos]['cwe']     = globalVars.NO_INFO
        #try:
        #    result1[pos]['reason']  = str(result1[pos]['reason'])
        #except  Exception:
        #    result1[pos]['reason']  = globalVars.NO_INFO
        #try:
        #    result1[pos]['summary'] = str(result1[pos]['summary'])
        #except  Exception:
        #    result1[pos]['summary'] = globalVars.NO_INFO

        countImpact(result1[pos])
        pos = pos + 1



def get_impact():
    message = "Hi!\n\nThe system has been successfully executed. You can check the vulnerabilities found in your command console.\n\n" \
            + "Impact summary:\n\n* Availability:\n\n    - NONE: " + str(aval1) + "\n\n    - PARTIAL: " + str(aval2) + "\n\n    - COMPLETE: " + str(aval3) + "\n\n    - NO INFO: " + str(aval4) \
            + "\n\n* Confidentiality:\n\n    - NONE: " + str(conf1) + "\n\n    - PARTIAL: " + str(conf2) + "\n\n    - COMPLETE: " + str(conf3) + "\n\n    - NO INFO: "+ str(conf4) \
            + "\n\n* Integrity:\n\n    - NONE: " + str(inte1) + "\n\n    - PARTIAL: " + str(inte2) + "\n\n    - COMPLETE: " + str(inte3) + "\n\n    - NO INFO: "+ str(inte4) \
            + "\n\n\nThank you. \n\n CETA Alert System "
    return message



def countImpact(result_pos):
    global aval1 
    global aval2
    global aval3
    global aval4
    global conf1
    global conf2
    global conf3
    global conf4
    global inte1
    global inte2
    global inte3
    global inte4
    
    try:
        if result_pos['impact']:
            if result_pos['impact']['availability']== 'NONE':
                aval1 = aval1 + 1
            else:
                if result_pos['impact']['availability']== 'PARTIAL':
                    aval2 = aval2 + 1
                else:
                    if result_pos['impact']['availability']== 'COMPLETE':
                        aval3 = aval3 + 1
                    else:
                        if result_pos['impact']['availability'] == globalVars.NO_INFO:
                            aval4 = aval4 + 1
            
            if result_pos['impact']['confidentiality']== 'NONE':
                conf1 = conf1 + 1 
            else:
                if result_pos['impact']['confidentiality']== 'PARTIAL':
                    conf2 = conf2 + 1
                else:
                    if result_pos['impact']['confidentiality']== 'COMPLETE':
                        conf3 = conf3 + 1
                    else:
                        if result_pos['impact']['confidentiality'] == globalVars.NO_INFO:
                            conf4 = conf4 + 1    
        
            if result_pos['impact']['integrity']== 'NONE':
                inte1 = inte1 + 1 
            else:
                if result_pos['impact']['integrity']== 'PARTIAL':
                    inte2 = inte2 + 1
                else:
                    if result_pos['impact']['integrity']== 'COMPLETE':
                        inte3 = inte3 + 1
                    else:
                        if result_pos['impact']['integrity'] == globalVars.NO_INFO:
                            inte4 = inte4 + 1  
    except Exception as exception:
        print('Error: ', exception)
