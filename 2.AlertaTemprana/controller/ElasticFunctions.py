#Elastic Functions
'''
Created on Mar 6, 2018

@author: Monospow
'''
import elasticsearch as elastic
import utils.GlobalVariables as globalVars
import json
import requests
import warnings
warnings.filterwarnings('ignore')

es             = None
elasticCount   = 0



def getElastic():
    global es
    if not es:
        es = elastic.Elasticsearch([globalVars.URLElastic], verify_certs=True)
    return es


def getElasticCount():
    global elasticCount
    return elasticCount


def getElasticIndex(index):
    es  = getElastic()
    res = es.search(index=index, body={"query": {"match_all": {}}})
    for n in res:
        print (n)
    print(res)
    return res


def deleteElasticIndex(index):
    es = getElastic()
    if es.indices.exists(index=index):
        es.indices.delete(index=index, ignore=[400, 404])
        #deleteIndexAll(index)


def deleteIndexAll(index):
    session  = requests.session()
    response = session.clear(globalVars.URLElastic + '/_all')
    response.connection.close()


def existsIndex (index):
    es = getElastic()
    return es.indices.exists(index=index)


def setElasticMappings():
    session = requests.Session()
    headers = {
        'Content-Type': 'application/json',
        'kbn-xsrf': 'anything',
    }
    mapping = {
        "template": "*",
        "settings": {
            "number_of_shards": 1,
            "index.mapping.total_fields.limit": 9000
        },
        "mappings": {
            "doc": {
                "properties": {
                    "ip": {
                        "type": "ip"
                    },
                    "ip_int": {
                        "type": "long"
                    },
                    "shodan.location": {
                        "properties": {
                            "geo": {
                                "type": "geo_point"
                            },
                            "geohash": {
                                "type": "geo_point"
                            }
                        }
                    }
                }
            }
        }
    }
    session.post(globalVars.URLElasticMappingAlert, headers=headers, json=mapping)


def saveJson2Elastic (body):
    global elasticCount
    es   = getElastic()
    id_f = abs(body['shodan.hash'])
    try:
        es.index(index=globalVars.elasticIndexName, doc_type=globalVars.prefixDocType, id=id_f, body=body)
        elasticCount = elasticCount + 1
        return True
    except:
        return False


def saveJsonFile2Elastic(file, index):
    es = getElastic()
    with open(file, 'r') as f:
        for line in f:
            es.index(index=index, doc_type=globalVars.prefixDocType, body=json.loads(line))


def existsIndexElasticSearch(service_line):
    es   = getElastic()
    id_f = abs(service_line['shodan.hash'])
    try:
        exists = es.get(index=globalVars.elasticIndexName, doc_type=globalVars.prefixDocType, id=id_f)
    except Exception:
        return False
    return exists
