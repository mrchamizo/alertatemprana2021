# Processes to be carried out in Shodan
'''
Created on Feb 21, 2018

@author: Monospow
'''
import utils.SystemFunctions as systemFunctions
import utils.SystemProperties as systemproperties
import controller.ElasticFunctions as elasticFunctions
import utils.Shodan2Elastic as shodan2elastic
import controller.CVEFunctions as cveFunctions
import utils.GlobalVariables as globalVars
import dao.mongodb_manager as mongodb_manager
import pickle as pickle
import json
import sys
import warnings
warnings.filterwarnings('ignore')

counter         = 0
existsOnShodan  = False


def initCVEandMappings():
    cveFunctions.getCVE()
    elasticFunctions.setElasticMappings()


def searchAndSave2Elastic(companyName):
    global counter
    global existsOnShodan
    
    #init CVEs and Elastic Mappings
    initCVEandMappings()
    
    api   = systemproperties.get_shodanApikey()
    query = globalVars.prefixSearchByCompany + companyName
    
    if query != '':
        if elasticFunctions.existsIndex(globalVars.elasticIndexName):         
            existsOnShodan = True
            elasticFunctions.deleteElasticIndex(globalVars.elasticIndexName)
        
        result = getShodanPages(query, 1, api)  #get first page
        pages  = (result['total'] // globalVars.shodanResultsPerPage) + 1
        print("pages "+str(pages))
        for page in range(1, pages+1):
            loadPage2Elastic(result['matches'])
            print ("Page " + str(page))
            if pages >= page+1:
                result = getShodanPages(query, page+1, api)

        print ("Data imported correctly: %d" % counter)
        mongodb_manager.setKeyValue(globalVars.count, counter)



def loadPage2Elastic(founds):
    """Find and save matches into JSON"""
    global counter
    global existsOnShodan
    
    for found in founds:
        found = systemFunctions.emptyClean(found)
        found = cveFunctions.cveSearch(found)
        
        shodan2elastic.mappingsParsingCorrect(found)
        isCorrect = loadJson2Elastic(found)
        
        if isCorrect:
            counter = counter + 1
         



def loadJson2Elastic(service):
    global existsOnShodan
    jsontry = json.dumps(service)
    bodyT   = json.loads(jsontry)
    
    if existsOnShodan and (elasticFunctions.existsIndexElasticSearch(bodyT) == False):
        isCorrect = elasticFunctions.saveJson2Elastic(bodyT)
    else:
        isCorrect = elasticFunctions.saveJson2Elastic(bodyT)
    return isCorrect



def getShodanPages(query, page, api):
    try:
        result = api.search(query=query, minify=pickle.FALSE, page=page)
        print("Results found: %d" % result['total'])
    except:
        print("Unexpected error:", sys.exc_info()[0])
    finally:
        return result
