#!/bin/bash

#CHECK MEMORY
totalm=$(free -m | awk '/^Mem:/{print $2}')
minmem=3000

#PATHS
desktop="/home/U406577/Desktop/"
gitalertatemprana="/home/U406577/git/alertatemprana/"
versionMongo="4.4.4"
versionELK="7.11.2-linux-x86_64"
elasticsearch="elasticsearch-"$versionELK".tar.gz"
kibana="kibana-"$versionELK".tar.gz"
mongodb="mongodb-linux-x86_64-amazon-"$versionMongo".tgz"
kibanaPluginNetwork="7.11.2-1.tar.gz"

#URLs
URLelasticsearch="https://artifacts.elastic.co/downloads/elasticsearch"
URLkibana="https://artifacts.elastic.co/downloads/kibana"
#URLkibanaPluginNetwork="https://github.com/dlumbrer/kbn_network/releases/download/6.0.X-1"
URLkibanaPluginNetwork="https://github.com/dlumbrer/kbn_network/archive"
URLmongoDB="https://fastdl.mongodb.org/linux"
URLcvesearch="https://github.com/cve-search/cve-search.git"




#START SERVICES
function startServices() {
    if [ "$totalm" -gt "$minmem" ]; then
        
        echo "  -> Start CVE-Search"
        cd $desktop/cve-search
        #mate-terminal --title=MongoDB --geometry=80x4+1200+0 -x sh -c 'python3 index.py' &
        sh -c 'python3 web/index.py' &

     
        
    else
        echo "Min neccesary memory: $minmem"
    fi
}


#RUN CVE-SEARCH
function startCVESearch() {
    
    cd $desktop
    
    if [ ! -d "$desktop/cve-search" ]; then
        echo "  -> Install cve-search"
        git clone $URLcvesearch
        cd cve-search
        pip install -r requirements.txt
        
        #Populating the database
        echo "  -> Populating CVEs database"
        ./sbin/db_mgmt_capec.py -p
        echo "  -> Populating CPE database"
        ./sbin/db_mgmt_cpe_dictionary.py -p
        
        #Add the cross-references from NIST, Red Hat and other vendors
        echo "  -> Populating cross-references from NIST, Red Hat and other vendors"
        ./sbin/db_mgmt_ref.py
    else
        cd cve-search
        echo "  -> Updating cve-search database"
        ./sbin/db_updater.py -c     
    fi
}


#MAIN
startCVESearch 	
#startServices
