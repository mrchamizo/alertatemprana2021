#!/bin/bash

#CHECK MEMORY
totalm=$(free -m | awk '/^Mem:/{print $2}')
minmem=3000

#PATHS
desktop="/home/U406577/Desktop/"
gitalertatemprana="/home/U406577/git/alertatemprana/"
versionMongo="4.4.4"
versionELK="7.12.0-linux-x86_64"
elasticsearch="elasticsearch-"$versionELK".tar.gz"
kibana="kibana-"$versionELK".tar.gz"
mongodb="mongodb-linux-x86_64-amazon-"$versionMongo".tgz"
kibanaPluginNetwork="7.12.0-1.tar.gz"

#URLs
URLelasticsearch="https://artifacts.elastic.co/downloads/elasticsearch"
URLkibana="https://artifacts.elastic.co/downloads/kibana"
#URLkibanaPluginNetwork="https://github.com/dlumbrer/kbn_network/releases/download/6.0.X-1"
URLkibanaPluginNetwork="https://github.com/dlumbrer/kbn_network/archive"
URLmongoDB="https://fastdl.mongodb.org/linux"
URLcvesearch="https://github.com/cve-search/cve-search.git"


#REMOVE ELASTIC AND KIBANA
function removeElasticKibana(){
    rm -rf $desktop/elasticsearch
    rm -rf $desktop/kibana
}


#INSTALL ELASTICSEARCH
function installElasticSearch() {
    if [ ! -d "$desktop/elasticsearch" ]; then
        echo "  -> Install elasticsearch"
 	cd $desktop
        wget $URLelasticsearch/$elasticsearch
        tar -xzf $elasticsearch
        rm $elasticsearch
        mv elasticsearch*/ elasticsearch
        sed -i 's/debug/off/g' ./elasticsearch/config/log4j2.properties           #change log level
        #Por dfecto instalado con elastic: https://www.elastic.co/guide/en/elasticsearch/plugins/current/ingest-geoip.html
        #installElasticPlugins
    fi
}


#INSTALL ELASTICSEARCH PLUGINS
function installElasticPlugins() {
    echo "  -> Install elasticsearch Plugins"
    ./elasticsearch/bin/elasticsearch-plugin install ingest-geoip #--batch
}


#INSTALL KIBANA
function installKibana() {
    if [ ! -d "$desktop/kibana" ]; then
        echo "  -> Install Kibana"
        cd $desktop
        wget $URLkibana/$kibana
        tar -xzf $kibana
        rm $kibana
        mv kibana*/ kibana
        installKibanaPlugins
        cd $desktop
        sed -i -e 's/"from": "now-15m"/"from": "now-1y"/g' ./kibana/src/core/types/ui_settings.js
        #sed -i -e 's/"from": "now-15m"/"from": "now-1y"/g' ./kibana/src/core_plugins/kibana/ui_setting_defaults.js   #change getUiSettingDefaults visualization
    fi
}


#INSTALL KIBANA PLUGINS
function installKibanaPlugins() {
    echo "  -> Install Kibana Plugins"
    cd $desktop/kibana/plugins
    git clone https://github.com/dlumbrer/kbn_network.git network_vis -b 7-dev
    cd network_vis
    rm -rf images/
    npm install
    #tar -xzf $kibanaPluginNetwork -C ./kibana/plugins
    #rm $kibanaPluginNetwork
}


#INSTALL MONGODB
function installMongoDB() {
    if [ ! -d "$desktop/mongodb" ]; then
        echo "  -> Install MongoDB"
        cd $desktop
        wget $URLmongoDB/$mongodb
        tar -xzvf $mongodb
        rm $mongodb
        mv mongodb-linux-*/ mongodb
        mkdir -p ./mongodb/data/db
    fi
	echo "  -> Running MongoDB"
        cd $desktop/mongodb
        mate-terminal --title=MongoDB --geometry=80x4+1200+0 -x sh -c './bin/mongod --dbpath ../mongodb/data/' &
        #sh -c './bin/mongod --dbpath ../mongodb/data/' &
}


#INSTALL CVESEARCH
function installCVESearch() {
    if [ ! -d "$desktop/cve-search" ]; then
	echo "  -> Install cve-search"
	git clone $URLcvesearch
	cd cve-search
        #mv etc/plugins.txt.sample etc/plugins.txt
	pip install -r requirements.txt

	#Populating the database
	echo "  -> Populating CVEs database"
	./sbin/db_mgmt_capec.py -p
	echo "  -> Populating CPE database"
	./sbin/db_mgmt_cpe_dictionary.py -p

	#Add the cross-references from NIST, Red Hat and other vendors
	echo "  -> Populating cross-references from NIST, Red Hat and other vendors"
	./sbin/db_mgmt_ref.py
    else
	echo "  -> CVE-Search ya está instalado"
    fi	
}


#RUN CVE-SEARCH
function startCVESearch() {
    cd $desktop
    if [ ! -d "$desktop/cve-search" ]; then
        echo "  -> Install cve-search"
        git clone $URLcvesearch
        cd cve-search
        pip install -r requirements.txt
        
        #Populating the database
        echo "  -> Populating CVEs database"
        ./sbin/db_mgmt_capec.py -p
        echo "  -> Populating CPE database"
        ./sbin/db_mgmt_cpe_dictionary.py -p
        
        #Add the cross-references from NIST, Red Hat and other vendors
        echo "  -> Populating cross-references from NIST, Red Hat and other vendors"
        ./sbin/db_mgmt_ref.py
    else
        cd cve-search
        echo "  -> Updating cve-search database"
        ./sbin/db_updater.py -c     
    fi
	echo "  -> Start CVE-Search"
	cd $desktop/cve-search
	#mate-terminal --title=MongoDB --geometry=80x4+1200+0 -x sh -c 'python3 web/index.py' &
	sh -c 'python3 web/index.py' &
}


#START SERVICES
function startServices() {
    if [ "$totalm" -gt "$minmem" ]; then

	echo "  -> Running elasticsearch"
        cd $desktop/elasticsearch
        mate-terminal --title=Elastic --geometry=80x4+1200+140 -x sh -c './bin/elasticsearch' &
        #sh -c './bin/elasticsearch' &
        
        echo "  -> Running kibana"
        cd $desktop/kibana
        mate-terminal --title=Kibana --geometry=80x4+1200+280 -x sh -c './bin/kibana' &
        #sh -c './bin/kibana' &

        #ALERTA TEMPRANA BOT
        echo "  -> Running Telegram Bot"
        cd $gitalertatemprana/3.AlertaTempranaBot
        mate-terminal --title=TelegramBot --geometry=80x4+1200+420 -x sh -c 'python3 TelegramBotMain.py' &
        #sh -c 'python3 TelegramBotMain.py' &
        
    else
        echo "Min neccesary memory: $minmem"
    fi
}


#MAIN
installElasticSearch
installKibana
installMongoDB
installCVESearch
startServices
startCVESearch
