#!/bin/bash

#RUN FROM THE TERMINAL

#System Requirements:
sudo yum -y install qt5-designer                                                #GUI designer
sudo yum -y install java-1.8.0-openjdk                                          #Java JDK need for elasticsearch
sudo yum -y install git                                                         #Distributed version control system
sudo yum -y install glibc*                                                      #Robomongo library
sudo yum --enablerepo=extras install epel-release				#This command will install the correct EPEL repository for the CentOS version you are running
sudo yum -y install python3-pip                                                 #Install and manage software packages written in Python

#sudo yum -y install python34-setuptools                                        #Install pip3
sudo easy_install-3 pip                                                       	#Install pip3
#WEB INSTALACIÓN CONDA https://linuxize.com/post/how-to-install-anaconda-on-centos-7/
conda update --all                                                         #python libs updater
sudo pip3 install --upgrade pip                                                  #Upgrade pip
pip install --upgrade pip
sudo yum install compat-openssl10

sudo yum install redis
sudo systemctl start redis
sudo systemctl enable redis
sudo systemctl status redis

#TELEGRAM BOT
pip uninstall telebot
pip uninstall PyTelegramBotAPI
pip install PyTelegramBotAPI

#PYQT5
#pip install pyqt5
sudo yum install PyQt5
pip install PyQtWebEngine


#Python libraries
mate-terminal --title="Python Requirements" --geometry=80x4+1200+0 -x sh -c 'pip install shodan elasticsearch simplejson python-crontab pymongo redis pyTelegramBotAPI telebot pymongo Pastebin'


#WEB INSTALACIÓN MONGO https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/
#sudo yum install -y mongodb-org
#sudo systemctl stop mongod.service



#Links for tools
#sudo ln -s /home/auditor/Desktop/mongodb/bin/mongo /usr/local/bin/mongo        #link mongo
#sudo ln -s /home/auditor/anaconda3/bin/conda /usr/bin/conda                    #link conda
