#URLs
URLElastic                  = "http://localhost:9200"

#Properties
pathAlerts                  = "/properties/alerts.properties"
pathMongoDB                 = "/properties/mongodb.properties"

#PATHS
pathOutput                  = "/outputs/"


#Constants
key                         = "KEY"
value                       = "VALUE"
impact                      = "impact"
alert                       = "alert"
cid                         = "cid"

#MESSAGES
telegramHelpGeneric         = "Available Commands:\n/enter - Login in the CETA system\n/info - Information System"
telegramHelpRegister        = "Available Commands: \n/count - vulnerabilities found in the last scan \n/impact - impact found in the last scan \n/alert0 - Disabled notify next scan finished\n/alert1 - Enabled notify next scan finished\n/logout - Logout "
telegramInfo                = "CETA - Early Warning System for Enterprise Vulnerabilities. Restricted access."
telegramInfoNotRegistered   = "You are not registered in Ceta. First you need to log in"
telegramAlertDisabled       = "You have disabled the alert"
telegramAlertEnabled       = "You have enabled the alert"