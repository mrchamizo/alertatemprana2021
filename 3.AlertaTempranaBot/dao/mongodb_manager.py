#!/usr/bin/python3
# -*- coding: utf-8 -*-
import configparser
import utils.GlobalVariables as globalVars
import dao.mongodb_connector as mongodbconnector
import os

#PROJECT PATH
projectPath  = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

#Read Properties
configMongoDB = configparser.ConfigParser()
configMongoDB.read(projectPath + globalVars.pathMongoDB)

#Read MongoDB Properties
hostmongo               = configMongoDB.get('mongoDB', 'hostmongo')
portmongo               = int(configMongoDB.get('mongoDB', 'portmongo'))
dbShodan                = configMongoDB.get('mongoDB', 'dbShodan')
colShodan               = configMongoDB.get('mongoDB', 'colShodan')

mongodbShodanSharedProperties  = None


def getShodanSharedProperties():
    global mongodbShodanSharedProperties
    if mongodbShodanSharedProperties is None:
        mongodbShodanSharedProperties = mongodbconnector.mongoDBConnection(hostmongo, portmongo, dbShodan, colShodan)
    return mongodbShodanSharedProperties


def getValue(key):
    value = ""
    mongodb = getShodanSharedProperties()
    try:
        value = mongodb.find_one({globalVars.key: key})[globalVars.value]
    except:
        print("Value of the property " + key + " not exists.") 
    return value


def setKeyValue(key, value):
    mongodb = getShodanSharedProperties()
    if (mongodb.find_one({globalVars.key: key})):
        mongodb.update_one({globalVars.key: key}, {"$set": {globalVars.value: value}}, upsert=False)
    else:
        data = {globalVars.key: key, globalVars.value: value}
        mongodb.insert(data)