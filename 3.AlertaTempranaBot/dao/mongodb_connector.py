#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import logging
from pymongo import MongoClient



def mongoDBConnection(host, port, database, collection):
    """MongoDB Connector"""
    
    try:
        connection = MongoClient(host, port)
        connection.server_info()        # force connection on a request
        db = connection[database]       #[collection]
        col = db[collection]
        
    except Exception as e:
        logging.error("Could not connect to MongoDB: %s" % e)
        sys.stderr.write("Could not connect to MongoDB: %s" % e)
        sys.exit(1)

    return col 