'''
Created on Apr 3, 2018

@author: Monospow
'''
import os
import configparser
import controller.ElasticProcess as elasticprocess
import utils.GlobalVariables as globalVars
import dao.mongodb_manager as mongodb_manager
import telebot

#PROJECT PATH
projectPath  = os.path.abspath(os.path.join(os.path.dirname(__file__)))


#Read File Properties
config = configparser.ConfigParser()
config.read(projectPath + globalVars.pathAlerts)

#Read Properties
TOKEN      = config.get('telegram', 'TOKEN')
admin_ceta = config.get('telegram', 'USER')
pass_ceta  = config.get('telegram', 'PWD')
bot        = telebot.TeleBot(TOKEN)     #Start Telegram Bot


#Constants
userCeta = ""
pwdCeta  = ""
enter    = 0
perCeta  = 0
userCeta = 0
alert    = 0



def listener(messages):
    '''A function called 'listener' is defined, which receives a parameter called 'messages' as a parameter.'''
    
    global userCeta
    global enter
    global userCeta
    global perCeta
    global TOKEN
    
    print("CETA Alert System Bot is running")
    
    for message in messages: 
        #The conversation ID is stored.
        cid      = message.chat.id
        username = message.from_user.username
        if message.content_type == 'text':
            if enter == 1:
                enter = 0
                if message.text == admin_ceta:
                    userCeta = 1
                    bot.send_message(cid, "Enter password. [pwd:password]")
                else:
                    bot.send_message(cid, "User error")
            else:
                if userCeta == 1:
                    userCeta = 0
                    if message.text == pass_ceta:
                        perCeta = 1
                        bot.send_message(cid, "You have accessed CETA")  
                    else:
                        bot.send_message(cid, "Password error")                      
            print ("[" + str(username) + "]: " + message.text)
            

bot.set_update_listener(listener)   #Bot is told to use the 'listener' 



@bot.message_handler(commands=['help']) 
def command_help(message): 
    global enter
    enter = 0
    cid = message.chat.id
    if perCeta == 1: 
        bot.send_message( cid, globalVars.telegramHelpRegister )
    else:
        bot.send_message( cid, globalVars.telegramHelpGeneric )
    
@bot.message_handler(commands=['enter'])
def command_user(message):
    global enter
    enter = 1
    cid = message.chat.id
    bot.send_message(cid, "Enter user:")
    
@bot.message_handler(commands=['info'])
def command_info(message):
    global enter
    enter = 0
    cid = message.chat.id
    bot.send_message(cid, globalVars.telegramInfo)
    
@bot.message_handler(commands=['count'])
def command_count(message):
    if perCeta == 1:
        cid = message.chat.id
        count= elasticprocess.get_count("")
        bot.send_message(cid, count)
    else:
        bot.send_message(cid, globalVars.telegramInfoNotRegistered) 

@bot.message_handler(commands=['impact'])
def command_impact(message):
    if perCeta == 1:
        cid    = message.chat.id
        impact = mongodb_manager.getValue(globalVars.impact)
        bot.send_message(cid, impact)
    else:
        try:
            bot.send_message(cid, globalVars.telegramInfoNotRegistered)
        except Exception:
            pass
        
@bot.message_handler(commands=['alert0'])
def command_alert0(message):
    global posts
    global alert
    if perCeta == 1:
        alert = "0"
        mongodb_manager.setKeyValue(globalVars.alert, alert)
        cid = message.chat.id
        mongodb_manager.setKeyValue(globalVars.cid, str(cid))
        bot.send_message(cid, globalVars.telegramAlertDisabled)
    else:
        bot.send_message(cid, globalVars.telegramInfoNotRegistered)

@bot.message_handler(commands=['alert1'])
def command_alert1(message):
    global posts
    global alert
    global cid
    if perCeta == 1:
        alert = "1"
        mongodb_manager.setKeyValue(globalVars.alert, alert)
        cid = message.chat.id
        mongodb_manager.setKeyValue(globalVars.cid, str(cid))
        bot.send_message(cid, globalVars.telegramAlertEnabled)
    else:
        bot.send_message(cid, globalVars.telegramInfoNotRegistered)

@bot.message_handler(commands=['logout'])
def command_logout(message):
    global perCeta
    if perCeta == 1:
        perCeta = 0
        cid = message.chat.id
        bot.send_message(cid, "User CETA logout")
    else:
        bot.send_message(cid, globalVars.telegramInfoNotRegistered)


bot.polling(none_stop=True)     #The bot keep running even if it finds a fault