'''
Created on Apr 9, 2018

@author: Monospow
'''
import elasticsearch as elastic
import utils.GlobalVariables as globalVars

res ={}

def get_elastic_index(index):
    global res
    es = elastic.Elasticsearch(globalVars.URLElastic, verify_certs=True)
    res = es.search(index=index, body={"query": {"match_all": {}}})
    return res


def get_count(index):
    
    if index == "":
        index = "shodan"
    res = get_elastic_index("shodan")
    return res['hits']['total']