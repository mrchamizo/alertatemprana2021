# CETA - Cyber Early Threat Alarm


## What is CETA?

CETA is an early warning system for vulnerabilities focused on organizations and vulnerabilities.


It is developed in python3 and uses the Shodan API to perform the vulnerability search of an organization and / or company. 

To do it, it has been used:

- Data processing is done with the [ELK Stack](https://www.elastic.co/webinars/introduction-elk-stack).
- Use the [CVE-Search](https://github.com/cve-search/cve-search) API to search for vulnerabilities without the need to be connected.
- Use [MongoDB](https://www.mongodb.com/) for data storage.
- Use [pyTelegramBotAPI](https://github.com/eternnoir/pyTelegramBotAPI) API for working with Telegram Bot


## Download

Download the latest version of the repository on Desktop.

 $ git clone https://monospow@bitbucket.org/monospow/alertatemprana.git

 
## Requirements

CETA has been developed on Centos 7. It's possible it should work in other distributions, but it is possible that some script fails.

To install the requirements, it is only necessary to execute the requirements.sh script:

    $ node --version
    v0.10.24

	
## Install & Start Services.

To start CETA it is only necessary to execute the script "StartServices.sh", this script will check if it is installed and it will start it. In case of missing any service (ELK, CVE-Search, PyTelegramApi, ...) it will download its latest version and will start, launching CETA.
   
   $ node --version
    v0.10.24

	
	

## Functioning.

It is only necessary to execute it, enter the name of the company, choose the operating mode and click on Start


## Note.

CETA is self-installable and self-configuring. It is ready to work.

